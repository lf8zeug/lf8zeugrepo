package de.szut.lf8_project.service;

import de.szut.lf8_project.exceptionHandling.ResourceNotFoundException;
import de.szut.lf8_project.model.Project;
import de.szut.lf8_project.repository.ProjectRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectService {

    private final ProjectRepository repository;

    public ProjectService(ProjectRepository repository) {
        this.repository = repository;
    }

    public Project create(Project newProject) {
        return repository.save(newProject);
    }


    public List<Project> readAll() {
        return repository.findAll();
    }

    public Project readById(long id) {
        Optional<Project> oProject = repository.findById(id);
        if (oProject.isEmpty()) {
            throw new ResourceNotFoundException("Project not found on id = " + id);
        }
        return oProject.get();
    }

    public void delete(long id) {
        repository.deleteById(id);
    }

	public Project update(Project updateProject) {
        return this.repository.save(updateProject);
    }
}
