package de.szut.lf8_project.service;

import de.szut.lf8_project.dto.AddProjectDTO;
import de.szut.lf8_project.dto.GetEmployeeProjectDataDTO;
import de.szut.lf8_project.dto.GetProjectDTO;
import de.szut.lf8_project.dto.GetProjectEmployeesDTO;
import de.szut.lf8_project.model.Project;
import de.szut.lf8_project.model.ProjectEmployee;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MappingService {

    public Project mapAddProjectDTOToProject(AddProjectDTO dto) {
        Project newProject = new Project();
        newProject.setDesignation(dto.getDesignation());
        newProject.setResponsibleEmployeeId(dto.getResponsibleEmployeeId());
        newProject.setCustomerId(dto.getCustomerId());
        newProject.setCustomerResponsiblePersonName(dto.getCustomerResponsiblePersonName());
        newProject.setGoalComment(dto.getGoalComment());
        newProject.setStartDate(dto.getStartDate());
        newProject.setPlannedEndDate(dto.getPlannedEndDate());
        newProject.setEndDate(dto.getEndDate());
        return newProject;
    }

    public GetProjectDTO mapProjectToGetProjectDTO(Project project){
        GetProjectDTO dto = new GetProjectDTO();
        dto.setId(project.getId());
        dto.setDesignation(project.getDesignation());
        dto.setResponsibleEmployeeId(project.getResponsibleEmployeeId());
        dto.setCustomerId(project.getCustomerId());
        dto.setCustomerResponsiblePersonName(project.getCustomerResponsiblePersonName());
        dto.setGoalComment(project.getGoalComment());
        dto.setStartDate(project.getStartDate());
        dto.setPlannedEndDate(project.getPlannedEndDate());
        dto.setEndDate(project.getEndDate());
        dto.setEmployees(project.getEmployees());
        return dto;
    }

    public GetEmployeeProjectDataDTO mapProjectToGetEmployeeProjectDataDTO(Project project, long employeeId){
        GetEmployeeProjectDataDTO dto = new GetEmployeeProjectDataDTO();
        dto.setId(project.getId());
        dto.setDesignation(project.getDesignation());
        dto.setStartDate(project.getStartDate());
        dto.setEndDate(project.getEndDate());
        ProjectEmployee pE = project.getEmployees().stream().filter(p -> p.getEmployeeId().equals(employeeId)).findFirst().orElse(null);
        if (pE==null){
            return null;
        }
        dto.setQualifications(pE.getQualifications());
        return dto;
    }

    public GetProjectEmployeesDTO mapProjectToGetProjectEmployeesDTO(Project project){
        GetProjectEmployeesDTO dto = new GetProjectEmployeesDTO();
        dto.setId(project.getId());
        dto.setDesignation(project.getDesignation());
        dto.setEmployees(project.getEmployees());
        return dto;
    }

}
