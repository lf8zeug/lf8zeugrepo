package de.szut.lf8_project.dto;

import de.szut.lf8_project.model.ProjectEmployee;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@Data
public class AddProjectDTO {

    @NotBlank(message = "Designation is mandatory")
    @Size(max = 500, message = "Designation must not exceed 500 characters")
    private String designation;

    @NotNull(message = "Responsible employee id is mandatory")
    private long responsibleEmployeeId;

    @NotNull(message = "Customer id is mandatory")
    private long customerId;

    @NotBlank(message = "Responsible employee name is mandatory")
    @Size(max = 50, message = "Responsible employee name must not exceed 50 characters")
    private String customerResponsiblePersonName;

    @NotBlank(message = "Goal comment is mandatory")
    @Size(max = 500, message = "Goal comment must not exceed 500 characters")
    private String goalComment;

    @NotNull
    private LocalDate startDate;

    @NotNull
    private LocalDate plannedEndDate;

    private LocalDate endDate;
}

