package de.szut.lf8_project.dto;

import de.szut.lf8_project.model.ProjectEmployee;
import lombok.Data;

import java.util.List;

@Data
public class GetProjectEmployeesDTO {

    private Long id;

    private String designation;

    private List<ProjectEmployee> employees;
}