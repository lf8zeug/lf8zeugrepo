package de.szut.lf8_project.dto;

import de.szut.lf8_project.model.ProjectEmployee;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class GetProjectDTO {

    private long id;

    private String designation;

    private long responsibleEmployeeId;

    private long customerId;

    private String customerResponsiblePersonName;

    private String goalComment;

    private LocalDate startDate;

    private LocalDate plannedEndDate;

    private LocalDate endDate;

    private List<ProjectEmployee> employees;
}
