package de.szut.lf8_project.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
public class AddProjectEmployeeDTO {

    @NotNull(message = "EmployeeId is mandatory")
    private Long employeeId;

    @NotNull(message = "Qualifications are mandatory")
    private Set<String> qualifications;
}