package de.szut.lf8_project.dto;

import lombok.Data;

import java.util.List;

@Data
public class GetEmployeeProjectsDTO {

    private long employeeId;

    List<GetEmployeeProjectDataDTO> projectData;
}