package de.szut.lf8_project.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.Set;

@Data
public class GetEmployeeProjectDataDTO {

    private long id;

    private String designation;

    private LocalDate startDate;

    private LocalDate endDate;

    private Set<String> qualifications;
}
