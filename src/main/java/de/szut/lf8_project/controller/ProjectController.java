package de.szut.lf8_project.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatchException;
import com.github.fge.jsonpatch.mergepatch.JsonMergePatch;
import de.szut.lf8_project.dto.*;
import de.szut.lf8_project.dto.GetProjectEmployeesDTO;
import de.szut.lf8_project.model.Project;
import de.szut.lf8_project.model.ProjectEmployee;
import de.szut.lf8_project.service.MappingService;
import de.szut.lf8_project.service.ProjectService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import javax.validation.Valid;


@RestController
@RequestMapping("/projectAdministration/project")
public class ProjectController {

    private final ProjectService service;

    private final MappingService mappingService;

    public ProjectController(ProjectService service, MappingService mappingService) {
        this.service = service;
        this.mappingService = mappingService;
    }

    @Operation(summary = "delivers a list of projects")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "list of projects",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetProjectDTO.class))}),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)
    })
    @GetMapping
    public ResponseEntity<List<GetProjectDTO>> findAllProjects() {
        List<Project> all = this.service.readAll();
        List<GetProjectDTO> dtoList = new LinkedList<>();
        for (Project project : all) {
            dtoList.add(this.mappingService.mapProjectToGetProjectDTO(project));
        }
        return new ResponseEntity<>(dtoList, HttpStatus.OK);
    }

    @Operation(summary = "delivers a project of a specific Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "project with id",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetProjectDTO.class))}),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "project with id not found",
                    content = @Content)
    })
    @GetMapping("/{id}")
    public ResponseEntity<GetProjectDTO> getProjectById(@PathVariable final Long id) {
        final var entity = this.service.readById(id);
        final GetProjectDTO dto = this.mappingService.mapProjectToGetProjectDTO(entity);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @Operation(summary = "creates a project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "project successfully created",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetProjectDTO.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "invalid parameters given",
                    content = @Content)
    })
    @PostMapping
    public ResponseEntity<GetProjectDTO> createProject(@Valid @RequestBody final AddProjectDTO dto, @RequestHeader("Authorization") String token) {
        if (!CustomerManagementController.customerExists(dto.getCustomerId(), token)) {
            throw new IllegalArgumentException("Customer id does not exist.");
        }
        if (!EmployeeManagementController.employeeExists(dto.getResponsibleEmployeeId(), token)) {
            throw new IllegalArgumentException("Responsible employee id does not exist.");
        }
        Project newProject = this.mappingService.mapAddProjectDTOToProject(dto);
        newProject = this.service.create(newProject);
        final GetProjectDTO request = this.mappingService.mapProjectToGetProjectDTO(newProject);
        return new ResponseEntity<>(request, HttpStatus.CREATED);
    }

    @Operation(summary = "updates the project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "updated project successfully",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetProjectDTO.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "project with id does not exists",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "invalid parameters given",
                    content = @Content)
    })
    @PutMapping("/{id}")
    public ResponseEntity<GetProjectDTO> updateProject(@PathVariable final Long id,
                                                       @Valid @RequestBody final AddProjectDTO updated,
                                                       @RequestHeader("Authorization") String token) {
        Project p = this.service.readById(id);
        if (!CustomerManagementController.customerExists(updated.getCustomerId(), token)) {
            throw new IllegalArgumentException("Customer id does not exist.");
        }
        if (!EmployeeManagementController.employeeExists(updated.getResponsibleEmployeeId(), token)) {
            throw new IllegalArgumentException("Responsible employee id does not exist.");
        }
        Project toUpdate = this.mappingService.mapAddProjectDTOToProject(updated);
        toUpdate.setId(id);
        toUpdate.setEmployees(p.getEmployees());
        Project newProject = this.service.update(toUpdate);
        final GetProjectDTO request = this.mappingService.mapProjectToGetProjectDTO(newProject);

        return new ResponseEntity<>(request, HttpStatus.OK);
    }

    @Operation(summary = "deletes a project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "project deleted successfully",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetProjectDTO.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "project with id does not exist",
                    content = @Content)
    })
	@DeleteMapping("/{id}")
    public ResponseEntity<GetProjectDTO> deleteProject(@PathVariable final long id){
        final var entity = this.service.readById(id);
        this.service.delete(id);
        final GetProjectDTO dto = this.mappingService.mapProjectToGetProjectDTO(entity);

        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @Operation(summary = "adds an employee to a project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = " employee added to project",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetProjectDTO.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "project with id does not exist",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "invalid parameters given",
                    content = @Content)
    })
    @PutMapping("/employee/{id}")
    public ResponseEntity<GetProjectDTO> addEmployeeToProject(@PathVariable final Long id,
                                                              @Valid @RequestBody final AddProjectEmployeeDTO employee,
                                                              @RequestHeader("Authorization") String token) {
        Project project = this.service.readById(id);
        if (!EmployeeManagementController.employeeExists(employee.getEmployeeId(), token)) {
            throw new IllegalArgumentException("Employee id does not exist.");
        }
        if (employee.getQualifications().isEmpty()){
            throw new IllegalArgumentException("No qualification was specified.");
        }
        for (String s: employee.getQualifications()){
            if (!EmployeeManagementController.employeeHasQualification(employee.getEmployeeId(),s,token)){
                throw new IllegalArgumentException("Employee does not have the "+ s + " qualification.");
            }
        }
        List<Project> all = this.service.readAll();
        Predicate<ProjectEmployee> pred = pE -> pE.getEmployeeId().equals(employee.getEmployeeId());
        for (Project p: all){
            if (p.getId()!=id&&p.getEmployees().stream().anyMatch(pred)){
                if (!(project.getStartDate().isAfter(p.getPlannedEndDate()))&&!(p.getStartDate().isAfter(project.getPlannedEndDate()))){
                    throw new IllegalArgumentException("Project time overlaps with project " + p.getId()+".");
                }
            }
        }
        List<ProjectEmployee> employees = project.getEmployees();
        for (ProjectEmployee emp: employees){
            if (emp.getEmployeeId().equals(employee.getEmployeeId())){
                emp.getQualifications().addAll(employee.getQualifications());
                Project newProject = this.service.update(project);
                final GetProjectDTO request = this.mappingService.mapProjectToGetProjectDTO(newProject);
                return new ResponseEntity<>(request, HttpStatus.OK);
            }
        }
        ProjectEmployee newEmployee = new ProjectEmployee();
        newEmployee.setEmployeeId(employee.getEmployeeId());
        newEmployee.setQualifications(employee.getQualifications());
        employees.add(newEmployee);
        Project newProject = this.service.update(project);
        final GetProjectDTO request = this.mappingService.mapProjectToGetProjectDTO(newProject);
        return new ResponseEntity<>(request, HttpStatus.OK);
    }

    @Operation(summary = "patches a project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "patched project successfully",
                    content = {@Content(mediaType = "merge-patch+json")}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "project with id does not exist",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "patch content holds invalid project data",
                    content = @Content)
    })
    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH, consumes = {"application/merge-patch+json"})
    public ResponseEntity<GetProjectDTO> patchProject(@PathVariable final Long id,
                                                      @Valid @RequestBody final String patch,
                                                      @RequestHeader("Authorization") String token) {
        Project p = this.service.readById(id);

        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.convertValue(p, JsonNode.class);
        JsonNode patchNode = null;
        try {
            patchNode = mapper.readTree(patch);
            JsonMergePatch mergePatch = JsonMergePatch.fromJson(patchNode);
            node = mergePatch.apply(node);
            p = mapper.treeToValue(node, Project.class);
        } catch (JsonProcessingException | JsonPatchException e) {
            throw new IllegalArgumentException("Sent JSON for patch is invalid.");
        }
        if (!CustomerManagementController.customerExists(p.getCustomerId(), token)) {
            throw new IllegalArgumentException("Customer id does not exist.");
        }
        if (!EmployeeManagementController.employeeExists(p.getResponsibleEmployeeId(), token)) {
            throw new IllegalArgumentException("Responsible employee id does not exist.");
        }
        Project newProject = this.service.update(p);
        final GetProjectDTO request = this.mappingService.mapProjectToGetProjectDTO(newProject);
        return new ResponseEntity<>(request, HttpStatus.OK);
    }

    @Operation(summary = "removes an employee from a project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "employee successfully removed from project",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetProjectDTO.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "project with id does not exist",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "invalid parameters given",
                    content = @Content)
    })
    @DeleteMapping("/employee/{id}")
    public ResponseEntity<GetProjectDTO> removeEmployeeFromProject(@PathVariable final Long id,
                                                                   @Valid @RequestBody final AddProjectEmployeeDTO employee) {
        Project project = this.service.readById(id);
        Predicate<ProjectEmployee> pred = pE -> pE.getEmployeeId().equals(employee.getEmployeeId());
        if (!project.getEmployees().stream().anyMatch(pred)) {
            throw new IllegalArgumentException("Employee is not in this project.");
        }
        project.setEmployees(project.getEmployees().stream()
                .filter(pE -> !pE.getEmployeeId().equals(employee.getEmployeeId())).collect(Collectors.toList()));
        Project newProject = this.service.update(project);
        final GetProjectDTO request = this.mappingService.mapProjectToGetProjectDTO(newProject);
        return new ResponseEntity<>(request, HttpStatus.OK);
    }


    @Operation(summary = "delivers a list of all employees of the project by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "employee list successfully returned",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetProjectEmployeesDTO.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "project with id does not exist",
                    content = @Content)
    })
    @GetMapping("/employee/{id}")
    public ResponseEntity<GetProjectEmployeesDTO> getAllEmployeesByProjectId(@PathVariable final Long id) {
        final var entity = this.service.readById(id);
        return new ResponseEntity<>(mappingService.mapProjectToGetProjectEmployeesDTO(entity), HttpStatus.OK);
    }

    @Operation(summary = "delivers all projects of the employee by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "project successfully returned",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetEmployeeProjectsDTO.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "project with id does not exist",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "invalid parameters given",
                    content = @Content)
    })
	@GetMapping("/employee/projects/{id}")
    public ResponseEntity<GetEmployeeProjectsDTO> getProjectsByEmployeeID(@PathVariable final Long id,
                                                                       @RequestHeader("Authorization") String token){
        if (!EmployeeManagementController.employeeExists(id, token)) {
            throw new IllegalArgumentException("Employee id does not exist.");
        }
        List<Project> all = this.service.readAll();
        GetEmployeeProjectsDTO res = new GetEmployeeProjectsDTO();
        res.setEmployeeId(id);
        List<GetEmployeeProjectDataDTO> projectData = new LinkedList<>();

        for (Project project : all) {
            GetEmployeeProjectDataDTO d = mappingService.mapProjectToGetEmployeeProjectDataDTO(project, id);
            if(d != null){
                projectData.add(d);
            }
        }
        res.setProjectData(projectData);
        return new ResponseEntity<>(res, HttpStatus.OK);
    }
}
