package de.szut.lf8_project.controller;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.function.Predicate;

public class EmployeeManagementController {

    private static String baseUrl = "https://employee.szut.dev";

    private static HttpResponse createAndExecuteAuthorizedHttpGet(String url, String token) throws IOException {
        HttpGet httpGet = new HttpGet(url);
        httpGet.setHeader("Authorization",token);
        HttpClient httpclient = HttpClients.createDefault();
        return httpclient.execute(httpGet);
    }

    public static boolean employeeExists(long id, String token) {
        HttpResponse response = null;
        try {
            response = createAndExecuteAuthorizedHttpGet(baseUrl+"/employees/"+id, token);
        } catch (IOException e) {
            return false;
        }
        HttpEntity entity = response.getEntity();
        if (entity == null) {
            return false;
        }
        return response.getStatusLine().getStatusCode() == 200;
    }

    public static boolean employeeHasQualification(long id, String qualification, String token){
        HttpResponse response = null;
        try {
            response = createAndExecuteAuthorizedHttpGet(baseUrl+"/employees/"+id+"/qualifications", token);
        } catch (IOException e) {
            return false;
        }
        HttpEntity entity = response.getEntity();
        if (entity == null) {
            return false;
        }
        JSONParser parser = new JSONParser();
        JSONObject json = null;
        try {
            json = (JSONObject)parser.parse(new String(entity.getContent().readAllBytes()));
        } catch (ParseException | IOException e) {
            return false;
        }
        JSONArray skills = (JSONArray) json.get("skillSet");
        Predicate<JSONObject> p = j -> j.containsValue(qualification);
        return skills.stream().anyMatch(p);
    }
}
