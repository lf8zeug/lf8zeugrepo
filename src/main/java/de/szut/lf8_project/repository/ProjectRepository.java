package de.szut.lf8_project.repository;

import de.szut.lf8_project.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<Project, Long> {


}
