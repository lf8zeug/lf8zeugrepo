package de.szut.lf8_project.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotBlank(message = "Designation is mandatory")
    @Size(max = 500, message = "Designation must not exceed 500 characters")
    private String designation;

    @NotNull(message = "Responsible employee id is mandatory")
    private long responsibleEmployeeId;

    @NotNull(message = "Customer id is mandatory")
    private long customerId;

    @NotBlank(message = "Responsible employee name is mandatory")
    @Size(max = 50, message = "Responsible employee name must not exceed 50 characters")
    private String customerResponsiblePersonName;

    @NotBlank(message = "Goal comment is mandatory")
    @Size(max = 500, message = "Goal comment must not exceed 500 characters")
    private String goalComment;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @NotNull
    private LocalDate startDate;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @NotNull
    private LocalDate plannedEndDate;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate endDate;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    private List<ProjectEmployee> employees = new ArrayList<>();

}
